## Running the website

The webpage needs to be run on a local server in order to be able to access the local files.
Using [ws][wslink] is recommended as it is a fast and simple solution to test the webpage.
Make sure that [Node.js][nodelink] is installed before installing **ws**.

If all requirements are installed open a terminal and browse to the folder where the
files of the webpage are in. Simply type **ws** and the webserver will be running.
An IP address will be displayed by which the site can be accessed in a browser.

## Things to note

### General

The content should adjust the size to the browser window. If it is not correctly sized
open the config and click _Show visualization_ again and the graph should adjust itself.

The 3d scatter plot takes some time to load.

### Heatmap

The heatmap is showing Fuzzy C-Means clustered data. There are several clusters and each word
is assigned a value from 0-1 for each cluster. This number indicates how much the word belongs
to the cluster. The higher the number the darker the color in the heatmap.

### Circular Dendrogram

The dendrogram can be zoomed and moved using the mouse.

### 3d Scatter Plot

The plot can be turned using the mouse. Doubeclick on a point of the plot to make it
the center of the rotation. 

### 2d Scatter Plot

The plot can be zoomed by holding down leftclick and moving the mouse. The marked 
area will be zoomed in. Zoom out by double clicking anywhere on the graph.


[wslink]: https://www.npmjs.com/package/ws
[nodelink]: https://nodejs.org/en/download/