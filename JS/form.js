$(window).on("load", function() {
   $("#formContainer").css("display", "block");
});

function showConfig() {
    $("#formContainer").fadeIn("slow");
    $("#showConfigButton").hide();
    $(this).trigger($.Event('showingConfig'));
}

function prepareGraphArea() {
    $("#graphContainer").empty();
}

$(document).ready(function() {
    $("input[name='vectoring']").on("change", function() {
        var radioValue = $(this).val();
        if(radioValue == "doc") {
            $("#word2vec").hide();
            $("#word2vec").find("input").prop("disabled", true);
            $("#doc2vec").show();
            $("#doc2vec").find("input").prop("disabled", false);
        } else {
            $("#doc2vec").hide();
            $("#doc2vec").find("input").prop("disabled", true);
            $("#word2vec").show();
            $("#word2vec").find("input").prop("disabled", false);
        }
    });
});

$("#myForm").on("submit", function(e) {
    e.preventDefault();
    e.stopPropagation();
    prepareGraphArea();
    // Setting a global variable which will be used by the data scripts as the path to the data files
    selectDataFile();
    var script = selectVisualisationScript();
    if (script === null)
        alert("Visualisation not implemented yet.");
    else {
        $("#formContainer").fadeOut("fast", function() {
            $.getScript(script);
        });
        $("#showConfigButton").show();
    }
});

/**
 * Selects the visualisation script based on the data file.
 * @returns {string}
 */
function selectVisualisationScript() {
    var alg = $("input[name='algorithm']:checked").val();
    var dim = $("input[name='vectorDimension']:checked").val();
    var out;
    switch (alg) {
        case "db":
            if(dim == "2d")
                out = "js/scatterPlot2d.js";
            else
                out = "js/scatterPlot3d.js";
            break;
        case "fcm":
            out = "js/heatmap.js";
            break;
        case "km":
            if(dim == "2d")
                out = "js/scatterPlot2d.js";
            else
                out = "js/scatterPlot3d.js";
            break;
        case "ward":
            out = "js/hierarchy.js";
            break;
        default:
            out = null;
            break;
    }
    return out;
}

function getFormValues() {
    var output = new Map();
    // get vectoring method
    var vec = $("input[name='vectoring']:checked").val();
    output.set("vec", vec);
    var dimension = $("input[name='vectorDimension']:checked").val();
    output.set("dimension", dimension);
    var alg = $("input[name='algorithm']:checked").val();
    output.set("alg", alg);
    if (vec == "word") {
        var dataset = $("input[name='dataset']:checked").val();
        output.set("dataset", dataset);
    } else {
        var dunno = $("input[name='dunno']:checked").val();
        output.set("dunno", dunno);
    }
    var textSize = $("input[name='txtSize']").val();
    output.set("textSize", textSize);
    return output;
}

function selectDataFile() {
    var path = "data_modified/";
    var formValues = getFormValues();
    var algo;
    switch (formValues.get("vec")) {
        case "word":
            path += "word2vec/";
            break;
        case "doc":
            path += "doc2vec/";
            break;
    }
    switch (formValues.get("alg")) {
        case "db":
            path += "dbscan/";
            algo = "_dbscan.csv";
            break;
        case "fcm":
            path += "fcm/";
            algo = "_fcm_m1_5.csv";
            break;
        case "km":
            path += "kmeans/";
            algo = "_kMeans.csv";
            break;
        case "ward":
            path += "ward/";
            algo = ".json";
            break;
    }
    if (("doc") == formValues.get("vec")) {
        path += "doc2vec_";
        switch (formValues.get("dunno")) {
            case "db":
                path += "dbow_";
                break;
            case "dm":
                path += "dm_";
                break;
        }
    } else {
        switch (formValues.get("dataset")) {
            case "all":
                path += "all_";
                break;
            case "hi":
                path += "hi_";
                break;
            case "int":
                path += "i_";
                break;
            case "lah":
                path += "lh_";
                break;
            case "mi":
                path += "mi_";
                break;
        }
    }
    switch (formValues.get("dimension")) {
        case "2d":
            path += "2d";
            break;
        case "3d":
            path += "3d";
            break;
    }
    path += algo;
    window.dataFileUrl = path;
}
