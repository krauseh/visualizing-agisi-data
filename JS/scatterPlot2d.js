$('link[href="scatterPlotStylesheet.css"]').prop('disabled', false);
// append the svg object to the body of the page
var margin = {top: 10, right: 30, bottom: 30, left: 60},
    width = $("#graphContainer > svg").width() - margin.left - margin.right,
    height = $("#graphContainer > svg").height() - margin.top - margin.bottom;

var svg = d3.select("#graphContainer")
    .append("svg")
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

resizeGraph();
function resizeGraph() {
    width = $("#graphContainer > svg").width() - margin.left - margin.right;
    height = $("#graphContainer > svg").height() - margin.top - margin.bottom;
    svg.attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");
}

//Read the data
d3.csv(dataFileUrl).then(function(data) {
    // Add X axis
    var x = d3.scaleLinear()
        .domain(d3.extent(data, function(d) { return parseFloat(d.x) }))
        .range([ 0, width ]);
    xAxis = svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

    // Add Y axis
    var y = d3.scaleLinear()
        .domain(d3.extent(data, function(d) { return parseFloat(d.y) }))
        .range([ height, 0]);
    svg.append("g")
        .call(d3.axisLeft(y));

    // Add a clipPath: everything out of this area won't be drawn.
    var clip = svg.append("defs").append("svg:clipPath")
        .attr("id", "clip")
        .append("svg:rect")
        .attr("width", width )
        .attr("height", height )
        .attr("x", 0)
        .attr("y", 0);

    // Color scale: give me a specie name, I return a color
    var color = d3.scaleOrdinal()
        .domain(["Cluster1", "Cluster2", "Cluster3", "Cluster4", "Cluster5", "Cluster6", "Cluster7", "Cluster8", "Cluster9", "Cluster10", "Noise" ])
        .range([ "#33cc33", "#1a75ff", "#fde725", "#ff3300", "#66ffff", "#cc00ff", "#ffad33", "#990000", "#006600", "#000000", "#ffccff"]);

    // Add brushing
    var brush = d3.brushX()                 // Add the brush feature using the d3.brush function
        .extent( [ [0,0], [width,height] ] ) // initialise the brush area: start at 0,0 and finishes at width,height: it means I select the whole graph area
        .on("end", updateChart) // Each time the brush selection changes, trigger the 'updateChart' function

    // Create the scatter variable: where both the circles and the brush take place
    var scatter = svg.append('g')
        .attr("clip-path", "url(#clip)");

    // Add dots
    scatter.selectAll("dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("class", function (d) { return "dot " + d.cluster } )
        .attr("cx", function (d) { return x(d.x); } )
        .attr("cy", function (d) { return y(d.y); } )
        .attr("r", 5)
        .style("fill", function (d) { return color(d.cluster) } );

    // Add the brushing
    scatter.append("g")
        .attr("class", "brush")
        .call(brush);

    // A function that set idleTimeOut to null
    var idleTimeout;
    function idled() { idleTimeout = null; }

    // A function that update the chart for given boundaries
    function updateChart() {

        extent = d3.event.selection;

        // If no selection, back to initial coordinate. Otherwise, update X axis domain
        if(!extent){
            if (!idleTimeout) return idleTimeout = setTimeout(idled, 350); // This allows to wait a little bit
            x.domain(d3.extent(data, function(d) { return parseFloat(d.x) }))
        }else{
            x.domain([ x.invert(extent[0]), x.invert(extent[1]) ]);
            scatter.select(".brush").call(brush.move, null) // This remove the grey brush area as soon as the selection has been done
        }

        // Update axis and circle position
        xAxis.transition().duration(1000).call(d3.axisBottom(x));
        scatter.selectAll("circle")
            .transition().duration(1000)
            .attr("cx", function (d) { return x(d.x); } )
            .attr("cy", function (d) { return y(d.y); } )

    }

    var usedColors = d3.map(data, function(d) { return(d.cluster) }).keys().sort();

// Add one dot in the legend for each name.
    svg.selectAll("mydots")
        .data(usedColors)
        .enter()
        .append("circle")
        .attr("cx", 100)
        .attr("cy", function(d,i){ return 100 + i*25}) // 100 is where the first dot appears. 25 is the distance between dots
        .attr("r", 7)
        .style("fill", function(d){ return color(d)})

// Add one dot in the legend for each name.
    svg.selectAll("mylabels")
        .data(usedColors)
        .enter()
        .append("text")
        .attr("x", 120)
        .attr("y", function(d,i){ return 100 + i*25}) // 100 is where the first dot appears. 25 is the distance between dots
        .style("fill", function(d){ return color(d)})
        .text(function(d){ return d})
        .attr("text-anchor", "left")
        .style("alignment-baseline", "middle")
});
