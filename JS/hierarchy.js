var svg = d3.select("#graphContainer")
    .append("svg")
    .call(d3.zoom().on("zoom", function() {
        svg.attr("transform", d3.event.transform);
    }));

g = svg.append("g");
resizeGraph();
$("#graphContainer").on("resize", function() {
    resizeGraph();
});

$("#showConfigButton").bind('showingConfig', function(e) {
    resizeGraph();
});

function resizeGraph() {
    width = $("#graphContainer > svg").width();
    height = $("#graphContainer > svg").height();
    g.attr("transform", "translate(" + (width / 2) + "," + (height / 2) + ")");
}

var cluster = d3.cluster()
    .size([360,390])
    .separation(function(x, y) { return (x.parent == y.parent ? 1 : 2) / x.depth; });

d3.json(dataFileUrl).then(function(data) {
    console.log(data);
    var test = d3.hierarchy(data);

    var nodes = cluster(test);

    console.log(nodes);
    var link = g.selectAll(".link")
        .data(nodes.descendants().slice(1))
        .enter().append("path")
        .attr("class", "link")
        .attr("d", function(d) {
            return "M" + project(d.x, d.y)
                + "C" + project(d.x, (d.y + d.parent.y) / 2)
                + " " + project(d.parent.x, (d.y + d.parent.y) / 2)
                + " " + project(d.parent.x, d.parent.y);
        });

    var node = g.selectAll(".node")
        .data(nodes.descendants())
        .enter().append("g")
        .attr("class", function(d) { return "node" + (d.children ? " node--internal" : " node--leaf"); })
        .attr("transform", function(d) { return "translate(" + project(d.x, d.y) + ")"; });

    /*            node.append("circle")
                    .attr("r", 2.5);*/

    node.append("text")
        .attr("dy", ".31em")
        .attr("x", function(d) { return d.x < 180 === !d.children ? 6 : -6; })
        .style("text-anchor", function(d) { return d.x < 180 === !d.children ? "start" : "end"; })
        .style("font-size", "5px")
        .attr("transform", function(d) { return "rotate(" + (d.x < 180 ? d.x - 90 : d.x + 90) + ")"; })
        .text(function(d) { return d.data.name; });
});

function project(x, y) {
    var angle = (x - 90) / 180 * Math.PI, radius = y;
    return [radius * Math.cos(angle), radius * Math.sin(angle)];
}
